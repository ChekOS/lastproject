const MSG_FIELD = "msg_field"; // identifier of the message input field
const MSG_BOX = "msg_box"; // identifier of the message box
const BASEURL = "https://chekos.northeurope.cloudapp.azure.com/lastproject/api"; // baseurl to send the messages
const SEPARATOR = ";";

/**
 * @typedef MESSAGE
 * @property {number} timestamp
 * @property {string} content
 */

/**
 * @type {Array<MESSAGE>}
 */
const Messages = [];

const fetchPromise = (endpoint, queries=[]) => new Promise((resolve, reject) => {
    try {
        const xhr = new XMLHttpRequest();
        xhr.withCredentials = true;
        xhr.addEventListener("readystatechange", function () {
            if (this.readyState === this.DONE) {
                // request is done, response from promise.
                resolve(this.responseText);
            }
        });
        if (queries.length > 0) {
            let queryprompt = "?";
            for (let i = 0; i < queries.length; i++) {
                const [key, value] = queries[i].split(";");
                queryprompt += key + "=" + value;
                if ((i - 1) !== queries.length) queryprompt += "&"; // more queryparams coming
            }
            xhr.open("GET", BASEURL+endpoint+queryprompt);
        } else {
            xhr.open("GET", BASEURL+endpoint);
        }
        xhr.send();
    } catch {
        reject();
    }
});

const sendMessage = () => {
    /**
     * This is type annotation in JavaScript
     * @type {HTMLInputElement}
     */
    const message_element = document.getElementById(MSG_FIELD);
    const msg = message_element.value;
    message_element.value = ""; // clear the input field after send click
    if (msg) {
        fetchPromise("/send-message", ["msg" + SEPARATOR + msg]);
    } else {
        alert("Empty message!");
    }
    
}

const fetchMessages = () => {
    Messages.splice(0,Messages.length); // clearing the list of messages
    fetchPromise("/get-messages").then((response) => {
        /** @type {Array<MESSAGE>} */
        const _messages = JSON.parse(response);
        Messages.push(..._messages); // filling the list with messages
        displayMessages(); // after data is fetched, make it visible to the user
    });
    return undefined;
}

const displayMessages = () => {
    /**
     * This is type annotation in JavaScript
     * @type {HTMLDivElement}
     */
    const msg_box = document.getElementById(MSG_BOX);
    msg_box.replaceChildren(); // removes all the messages
    Messages.reverse();
    for (const Message of Messages) { // pushing new messages to the GUI
        const para = document.createElement("div");
        para.setAttribute("id", "complete_msg");

        const dateMsg = document.createElement("div");
        dateMsg.setAttribute("id", "date_msg");
        dateMsg.innerText = Message.timestamp.slice(0,19);
        para.appendChild(dateMsg);

        const textMsg = document.createElement("div");
        textMsg.setAttribute("id", "text_msg");
        textMsg.innerText = Message.content;
        para.appendChild(textMsg);

        msg_box.appendChild(para);
    }
    return undefined;
}