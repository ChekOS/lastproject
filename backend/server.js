const express = require('express');

const PORT = 4501;

class Message {
    constructor(msg) {
        this.content = msg;
        this.timestamp = new Date().toISOString();
    }
    // this method takes in list of message instances
    // returns list of plain objects
    static objectify(messages) {
        const objects = [];
        for (const msg of messages) {
            objects.push(JSON.parse(JSON.stringify(msg)));
        }
        return objects;
    }
}

class Main {
    messages = [];
    constructor() {
        this.app = express();
        this.port = PORT;
        console.log('hello');
        this.createEndpoints();
        this.startServer();
    }
    createEndpoints() {
        this.app.get('/', (req, res) => {
            console.log('somebody calling server');
            res.send('OK');
        });
        this.app.get('/send-message', (req, res) => {
            const msg = new Message(req.query.msg);
            this.messages = [msg, ...this.messages];
            res.send('server got message.');
        });
        this.app.get('/get-messages', (req, res) => {
            console.log({msgs: this.messages});
            const plainMessages = Message.objectify(this.messages);
            res.json(plainMessages);
        });
        // below code serves the public directory contents as they are
        this.app.use('/static', express.static('public'));
    }
    startServer() {
        this.app.listen(this.port, () => console.log(`Listening: ${this.port}`));
    }
}

const main = new Main();